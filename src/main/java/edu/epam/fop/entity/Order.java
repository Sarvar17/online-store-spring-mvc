package edu.epam.fop.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "order_date")
    private String order_date;

    public Order() {
    }

    public Order(User userId, Product productId, int quantity, String order_date) {
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
        this.order_date = order_date;
    }
}

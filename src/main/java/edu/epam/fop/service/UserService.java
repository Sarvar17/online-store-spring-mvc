package edu.epam.fop.service;

import edu.epam.fop.dto.UserDTO;
import edu.epam.fop.entity.User;

import java.util.List;

public interface UserService {
    void saveUser(UserDTO userDTO);

    User findByUsername(String username);

    List<UserDTO> findAllUsers();
}
